from fabric.api import task, run, sudo
from fabric.operations import put
from fabric.contrib import files

try:
  import customconfig
except ImportError:
  class customconfig:
    key = None
    crt = None
  print "Not using a customconfig. To define a customconfig create a customconfig.py file next to this fabfily"

remote_homepage_dir = "/srv/www/homepage"
homepage_nginx_config_file = "/etc/nginx/sites-enabled/homepage"
homepage_nginx_ssl_dir = "/etc/nginx/ssl"

prod_nginx_configs = {
    "homepage_primary_server": "jessevogt.com",
    "homepage_server_aliases": "www.jessevogt.com jvogt.net www.jvogt.net",
    "labs_primary_server": "jv0.us"
}

test_nginx_configs = {
    "homepage_primary_server": "jessevogttest.com",
    "homepage_server_aliases": "www.jessevogttest.com",
    "labs_primary_server": "jv0test.us"
}

@task
def docker(env):
  context = None
  if env == 'test':
    context = test_nginx_configs
  if env == 'prod':
    context = prod_nginx_configs
  if context is None:
    raise Exception("Unspecified env")
  put("dist/*", remote_homepage_dir, use_sudo=True)
  sudo("mkdir -p /etc/docker-nginx/sites-enabled")
  files.upload_template("sites.conf", "/etc/docker-nginx/sites-enabled/sites.conf", context, use_sudo=True, backup=False)
  sudo("mkdir -p /etc/docker-nginx/ssl")
  if customconfig.key is not None:
    put(customconfig.key, "/etc/docker-nginx/ssl", use_sudo=True)
  if customconfig.crt is not None:
    put(customconfig.crt, "/etc/docker-nginx/ssl", use_sudo=True)
  sudo(("docker run -d " +
    "-p 80:80 -p 443:443 " +
    "-v {remote_homepage_dir}:{remote_homepage_dir}:ro " +
    "-v /etc/docker-nginx/sites-enabled:/etc/nginx/conf.d:ro " +
    "-v /etc/docker-nginx/ssl:/etc/nginx/ssl:ro " +
    "--link 'screeningportal:screeningportal' " +
    "--name nginx nginx").format(remote_homepage_dir=remote_homepage_dir))

@task
def website(env):
  context = None
  if env == 'test':
    context = test_nginx_configs
  if env == 'prod':
    context = prod_nginx_configs
  if context is None:
    raise Exception("Unspecified env")
  put("dist/*", remote_homepage_dir, use_sudo=True)
  files.upload_template("sites.conf", homepage_nginx_config_file, context, use_sudo=True, backup=False)
  sudoml("""
    chmod 644 {webdir}/*
    chmod -R 644 {conf}
    chown -R root:root {conf}
    chown -R root:root {webdir}
    nginx -s reload
  """, webdir=remote_homepage_dir, conf=homepage_nginx_config_file)


@task
def nginx():
  sudoml("""
    apt-get install -y software-properties-common
    add-apt-repository -y ppa:nginx/stable
    apt-get update
    apt-get install -y nginx
    apt-get upgrade -y nginx

    mkdir -p {ssldir}
    chown root:root {ssldir}
    chmod 700 {ssldir}
    rm -f /etc/nginx/sites-enabled/default

    mkdir -p {webdir}
    chmod 655 {webdir}
  """, webdir=remote_homepage_dir, ssldir=homepage_nginx_ssl_dir)
  if customconfig.key is not None:
    put(customconfig.key, homepage_nginx_ssl_dir, use_sudo=True)
  if customconfig.crt is not None:
    put(customconfig.crt, homepage_nginx_ssl_dir, use_sudo=True)


def sudoml(cmds, **kwargs):
  sudo(" && ".join(filter(len, [line.strip() for line in cmds.split("\n")])).format(**kwargs))

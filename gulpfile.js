'use strict';

var gulp = require('gulp'),
  less = require('gulp-less'),
  uncss = require('gulp-uncss'),
  cssmin = require('gulp-cssmin'),
  inlinesource = require('gulp-inline-source'),
  rev = require('gulp-rev'),
  template = require('gulp-template'),
  watch = require('gulp-watch'),
  livereload = require('gulp-livereload'),
  fs = require('fs'),
  _ = require('lodash');

var DIST = 'dist/';
var BUILD = 'build/';
var SRC = 'src/';

var LESS_FILES = [SRC + 'custom.less'];
var HTML_FILES = [SRC + '*.html'];
var IMAGE_FILES = ['8to5view.small.jpg', 'lake.small.jpg', 'jesse.200.jpg'].map(function(i){
  return SRC + i;
});

var TEMPLATE_DATA = {
  timestamp: new Date(),
  blogheader: (function() {
    var headerTemplate = fs.readFileSync(SRC + 'blog-post-header.html', "utf8");
    return function(params) {
      return header.replace(new RegExp('<!--%%TITLE%%-->', 'g'), params.title || "");
    };
  })(),
  blogfooter: (function() {
    var footer = fs.readFileSync(SRC + 'blog-post-footer.html', "utf8");
    return function(params) {
      return footer;
    };
  })()
};

gulp.task('css', function() {
  gulp.src(LESS_FILES)
  .pipe(less({
    paths: ['bower_components/bootstrap/less', SRC]
  }))
  .pipe(uncss({
    html: HTML_FILES
  }))
  .pipe(cssmin())
  .pipe(gulp.dest(BUILD));
});

(function(htmlModes) {
  htmlModes.forEach(function(mode) {
    gulp.task(mode.name, function() {
      var templateData = _.cloneDeep(TEMPLATE_DATA);
      templateData.enableLiveReload = !!mode.enableLiveReload;
      gulp.src(HTML_FILES)
      .pipe(template(templateData))
      .pipe(template(templateData))
      .pipe(inlinesource({
        swallowErrors: false
      }))
      .pipe(gulp.dest(DIST));
    });
  });
})([{name: 'html'}, {name:'html-watch', enableLiveReload: true}]);

gulp.task('images', function() {
  gulp.src(IMAGE_FILES)
  .pipe(rev())
  .pipe(gulp.dest(DIST));
});

gulp.task('misc', function() {
  gulp.src([SRC + 'favicon.ico'])
  .pipe(gulp.dest(DIST));
});

gulp.task('default', ['css', 'images', 'html', 'misc']);

gulp.task('watch', function() {
  livereload.listen();

  wrapWatch(HTML_FILES.concat([SRC + '*.less']), ['css', 'html-watch']);
  wrapWatch([DIST + '*'], undefined, livereload.changed);

  function wrapWatch(files, tasks, f) {
    gulp.watch(files, tasks).on('change', function(file) {
      console.log(file.path);
      if (f) f(file.path);
    });
  }
});
